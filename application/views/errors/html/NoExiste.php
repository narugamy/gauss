<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

	<div id="container">
		<h2 class="text-center">Valor no permitido</h2>
		<div class="row">
			<div class="col-xs-4"></div>
			<div class="col-xs-4 ">
				<a href="<?=base_url()?>" class="btn btn-primary btn-lg1 btn-block">Atras</a>
			</div>
		</div>
	</div>