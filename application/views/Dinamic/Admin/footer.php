</div>
    <div class="footer">
    	<div class="container">
    		<div class="col-md-3 grid_4">
    		   <h3>Nosotros</h3>	
    		   <p>"Somos una organización educativa que forma líderes con un alto nivel académico y una sólida formación en valores que les permita afrontar con éxito los retos de su vida personal y profesional."</p>
    		      <ul class="social-nav icons_2 clearfix">
    		     	<li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/kfgaussNorcav" target="_blank" class="facebook"> <i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
                 </ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Quick Links</h3>
    			<ul class="footer_list">
    				<li><a href="#">-  PRIOR LEARNING ASSESSMENT </a></li>
    				<li><a href="#">-  INTERNATIONAL STUDENTS</a></li>
    				<li><a href="#">-  CAREER OPPORTUNITIES</a></li>
    				<li><a href="#">-   NEW STUDENT ORIENTATION</a></li>
    				<li><a href="#">-   NEW CLASSROOM TECHNOLOGY</a></li>
    			</ul>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Contactanos</h3>
    			<address>
                    <strong>Direccion</strong>
                    <br>
                    <span>4877 It is a long established</span>
                    <br>
                    <strong>Datos Secundarios</strong>
                    <br>
                    <abbr>Telefono   : </abbr> (01) 3360326
                    <br>
                    <abbr>Email : </abbr> <a href="mailto@example.com">kfgauss@gmail.com</a>
               </address>
    		</div>
    		<div class="col-md-3 grid_4">
    		   <h3>Horario de Atención</h3>
    			 <table class="table_working_hours">
		        	<tbody>
		        		<tr class="opened_1">
							<td class="day_label">Lunes</td>
							<td class="day_value">7:30 am - 8.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Martes</td>
							<td class="day_value">7:30 am - 8.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Miercoles</td>
							<td class="day_value">7:30 am - 8.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Jueves</td>
							<td class="day_value">7:30 am - 8.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Viernes</td>
							<td class="day_value">7:30 am - 8.00 pm</td>
						</tr>
					    <tr class="opened">
							<td class="day_label">Sabado</td>
							<td class="day_value">7:30 am - 12.00 pm</td>
						</tr>
					    <tr class="closed">
							<td class="day_label">Domingo</td>
							<td class="day_value closed"><span>cERRADO</span></td>
						</tr>
				    </tbody>
				</table>
            </div>
    		<div class="clearfix"> </div>
    		<div class="copy">
		       <p>Derechos de autor © 2014 - Todos los derechos reservados Circulo de estudios K.f. Gauss | Design by <a href="http://www.youtube.com/channel/UCLKLzNebXrbKR9kq4x6de3Q" target="_blank">KirigayaM@ster</a> </p>
	        </div>
    	</div>
    </div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?= base_url()?>assets/S1/js/jquery.min.js"></script>
<script src="<?= base_url()?>assets/S1/js/script.js"></script>
<script src="<?= base_url()?>assets/S1/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/S1/js/responsiveslides.min.js"></script>
<script src="<?= base_url()?>assets/S1/js/jquery.countdown.js"></script>
</body>
</html>	