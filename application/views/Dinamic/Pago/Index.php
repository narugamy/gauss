<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  {
	  	redirect(base_url());?>
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">
	   		<div class="login">
                <h3>Listado de Opciones</h3>
                 <div class="form-group">
                    <span id="clock" class="btn btn-success relog">
                    </span>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>Pago/Registro" class="btn btn-primary btn-lg1 btn-block boton" >Registrar Nuevo Pago</a>
                </div>
               
                <div class="form-group">
                    <a href="<?=base_url()?>Pago/Buscar" class="btn btn-primary btn-lg1 btn-block boton" >Buscar Pago</a>
                </div>
            </div>
	   </div>
	</div>
	<?php }?>