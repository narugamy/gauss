<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else{ ?>
	<div class="courses_box1">
	   <div class="container">	
		    <div class="login">
		    	<?php
            	foreach($Pago as $pag)
            	{ ?>
		    	<p class="lead">Numero de Recibo: <?= $pag->NumeroRecibo?></p>
		    	<div class="form-group">
				    	<label for="usuario">Pago</label>
					    <input type="text" name="usuario" id="usuario" class='users form-control' value="<?= $pag->Cantidad?>" disabled='off'>
				</div>
				<div class="form-group">
				    	<label for="Cantidad">Pago</label>
					    <input type="text" name="Cantidad" id="Cantidad" class='users form-control' value="<?= $pag->Cantidad?>" disabled='off'>
				</div>
			    <div class="form-group">
			    	<label for="Fecha">Fecha de Pago</label>
				    <input type="date" name="Fecha" class='users form-control' id="Fecha" value="<?= $pag->Fecha?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Pago/Buscar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
			    </div>
			    <?php }?>
		    </div>
	    </div>
	</div>
	<?php }?>