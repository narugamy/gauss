<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form = array('class' => 'login','id'=>'Pago','role'=>'form');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  { 
	  	redirect(base_url());?>
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">
	   <?=form_open(base_url().'Pago/Registrar',$form)?>
	    	<p class="lead">Registro de Pago</p>
		    <div class="form-group">
		    	<label for="Persona">Codigo</label>
			    <select name="Persona" class="users form-control acc" id="Persona" required>
		    		<option value="">Seleccione una Opcion</option>
		    		<?php foreach ($alumno as $alum) {?>
		    			<option value="<?= $alum->idPersona ?>"><?= $alum->PrimerNombre."  ".$alum->ApellidoPaterno."  ".$alum->ApellidoMaterno?></option>
		    		<?php }?>
				</select>
			<p id="co"></p>
		    </div>
		    <div class="form-group">
		    	<label for="NumeroRecibo">Numero de Recibo</label>
			    <input type="number" name="NumeroRecibo" class='users form-control' placeholder='NumeroRecibo' value="" id="NumeroRecibo" min="0" step="1">
			    <p id="nr"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Cantidad">Cantidad</label>
			    <input type="number" name="Cantidad" class='users form-control' placeholder='Cantidad' value="" id="Cantidad" min="0" step="1">
			    <p id="ca"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Fecha">Fecha</label>
			    <input type="date" name="Fecha" class='users form-control' placeholder='Fecha' value="" id="Fecha">
			    <p id="fe"></p>
		    </div>
		    <div class="form-group">
		    	<input type="hidden" name="valor" value="3">
		    </div>
		    <div class="form-group">
			    <button type="submit" class="btn btn-primary btn-lg1 btn-block">Registrar</button>
		    </div>
		    <div class="form-group">
		    	<a href="<?=base_url()?>Persona" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
		    </div>
		    <div "form-group">
	    		<p id="Respuesta"></p>
	    	</div>
	    <?=form_close()?>
	   
	   </div>
	</div>
	<?php }?>