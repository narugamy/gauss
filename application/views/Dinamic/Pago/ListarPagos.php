<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
?>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   		<div class="login">
	       		<h3>Lista de Pagos Encontrados</h3>
	            <?php foreach ($Pago as $pag){?>
	            	<div class="form-group">
		            	<?php foreach($Alumno as $empl)
		            		{ ?>
	            		<a class="btn btn-success" value="<?= $empl->Codigo?>" role="buttohon"><?= $empl->PrimerNombre."  ".$empl->ApellidoPaterno."  ".$empl->ApellidoMaterno?></a>
	            		<?php }?>
		                <a href="<?=base_url()?>Pago/Pago" class="btn btn-success botones" value="<?= $pag->idPago?>" role="buttohon"><?= $pag->NumeroRecibo?></a>
		                <a href="<?=base_url()?>Pago/Editar" class="btn btn-warning botones" value="<?= $pag->idPago?>" role="buttohon">Editar</a>
		            </div>
		                <?php }?>
		            <hr>
	            <a href="<?=base_url()?>Pago/Buscar/" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
           </div>
	   </div>
	</div>