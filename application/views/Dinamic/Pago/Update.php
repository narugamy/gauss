<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin'){
		redirect(base_url());?>
<?php }else{ ?>
	<div class="courses_box1">
		<div class="container">
			<form action="<?=base_url()?>Pago/Update" method="post" class="login" id="update">
			<?php
            	foreach($Pago as $pag)
            	{ ?>
		    	<p class="lead">Numero de Recibo: <?= $pag->NumeroRecibo?></p>
		    	<div class="form-group">
					    <input type="hidden" name="idPago" id="idPago" class='users form-control' value="<?= $pag->idPago?>">
				</div>
				<div class="form-group">
					    <input type="hidden" name="valor" id="valor" class='users form-control' value="3">
				</div>
				<div class="form-group">
				    	<label for="Cantidad">Pago</label>
					    <input type="number" name="Cantidad" id="Cantidad" class='users form-control' min="0" step="1" value="<?= $pag->Cantidad?>">
				</div>
			    <div class="form-group">
			    	<label for="Fecha">Fecha de Pago</label>
				    <input type="date" name="Fecha" class='users form-control' id="Fecha"value="<?= $pag->Fecha?>">
			    </div>
			    <div class="form-group">
			    	<button type="submit" class="btn btn-primary btn-lg1 btn-block">Guardar</button>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Pago/Buscar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
			    </div>
			    <?php }?>
			</form>
		</div>
	</div>
	<?php }?>
	