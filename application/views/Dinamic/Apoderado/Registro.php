<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form = array('class' => 'login','id'=>'myformRegA','role'=>'form');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">	
	   <?=form_open(base_url().'Apoderado/Registrar',$form)?>
	    	<p class="lead">Registro de Apoderado</p>
		    <div class="form-group">
		    	<label for="Nombre">Nombre</label>
			    <input type="text" name="Nombre" class='users form-control' placeholder='Nombre' value="" id="Nombre">
			    <p id="no"></p>
		    </div>
		    <div class="form-group">
		    	<label for="PApellido">Apellido Parterno</label>
			    <input type="text" name="ApellidoPaterno" class='users form-control' placeholder='Apellido Parterno' value="" id="PApellido">
			    <p id="ap"></p>
		    </div>
		    <div class="form-group">
		    	<label for="SApellido">Apellido Materno</label>
			    <input type="text" name="ApellidoMaterno" class='users form-control' placeholder='Apellido Materno' value="" id="SApellido">
			    <p id="am"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Dni">Dni</label>
			    <input type="number" name="Dni" class='users form-control' placeholder='Dni' value"" id="Dni"  min="0" step="1" required>
			    <p id="dn"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Correo">Correo Electronico</label>
			    <input type="email" name="Correo" class='users form-control' placeholder='Correo Electronico' value="" id="Correo">
			    <p id="em"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Telefono">Telefono</label>
			    <input type="number" name="Telefono" class='users form-control' placeholder='Telefono' value="" id="Telefono"  min="0" step="1">
			    <p id="te"></p>
		    </div>
		    <div class="form-group">
		    	<input type="hidden" name="valor" value="3">
		    </div>
		    <div class="form-group">
			    <button type="submit" class="btn btn-primary btn-lg1 btn-block">Registrar</button>
		    </div>
		    <div class="form-group">
		    	<a href="<?=base_url()?>Persona" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
		    </div>
		    <div "form-group">
	    		<p id="Respuesta"></p>
	    	</div>
	    <?=form_close()?>
	   
	   </div>
	</div>
	<?php }?>