<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
?>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   		<div class="login">
	       		<h3>Lista de Apoderados Encontrados</h3>
	            <?php
	            	foreach($apoderado as $apod)
	            	{ ?>
	            	<div class="form-group">
		                <a href="<?=base_url()?>Apoderado/Apoderado" class="btn btn-success botones" value="<?= $apod->Dni?>" role="buttohon"><?= $apod->Nombre."  ".$apod->ApellidoPaterno?></a> 
		                <a href="<?=base_url()?>Apoderado/Editar" class="btn btn-warning botones" value="<?= $apod->Dni?>" role="buttohon">Editar</a>
		                <?php if($apod->Estado==1){?>
		                <a href="<?=base_url()?>Apoderado/Eliminar" class="btn btn-danger botones" value="<?= $apod->Dni?>" role="button">Eliminar</a>
		                <?php }else{?>
						<a href="<?=base_url()?>Apoderado/Habilitar" class="btn btn-primary botones" value="<?= $apod->Dni?>" role="button">Habilitar</a>
		                <?php }?>
		            </div>
		            
	          <?php }?>
	            <hr>
	            <a href="<?=base_url()?>Apoderado/Buscar/" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
           </div>
	   </div>
	</div>