<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">	
	   		<div class="login">
                <h3>Listado de Opciones</h3>
                <div class="form-group">
                    <a href="<?=base_url()?>Apoderado/registro" class="btn btn-primary btn-lg1 btn-block boton" >Registrar Nuevo Apoderado</a>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>Apoderado/buscar" class="btn btn-primary btn-lg1 btn-block boton" >Buscar Apoderado</a>
                </div>
            </div>
	   </div>
	</div>
	<?php }?>