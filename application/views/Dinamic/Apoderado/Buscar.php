<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin'){
		redirect(base_url());?>
<?php }else{ ?>
	<div class="courses_box1">
	   <div class="container">
		    <div class="login">
		    	<p class="lead">Bienvenido</p>
			    <div class="form-group">
			    	<label for="usuario">Codigo</label>
				    <input type="number" name="Dni" id="Dni" class='users form-control' placeholder='Dni' value=""  min="0" step="1">
			    </div>
			    <div class="form-group">
			    	<p id="error">
			    		<?php if(!empty($error)){
			    			echo $error;
			    		}?>
			    	</p>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Apoderado/Listar" class="btn btn-primary btn-lg1 btn-block" id="buscaral">Buscar</a>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Apoderado" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
			    </div>
		    </div>
	    </div>
	</div>
	<?php }?>