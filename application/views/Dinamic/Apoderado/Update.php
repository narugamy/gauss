<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin'){
		redirect(base_url());?>
<?php }else{ ?>
	<div class="courses_box1">
		<div class="container">
			<form action="<?=base_url()?>Apoderado/Update" method="post" class="login" id="update">
				<?php   foreach($apoderado as $apod){ ?>
				<p class="lead">Señor:  <?= $apod->Nombre."  ".$apod->ApellidoPaterno."  ".$apod->ApellidoMaterno?></p>
				<div class="form-group">
					<input type="hidden" name="Dni" id="usuario" class='users form-control' value="<?= $apod->Dni?>">
				</div>
				<div class="form-group">
					<input type="hidden" name="valor" id="valor" class='users form-control' value="3">
				</div>
				<div class="form-group">
					<label for="Nombre">Primer Nombre</label>
					<input type="text" name="Nombre" class='users form-control' value="<?= $apod->Nombre?>" id="Nombre" required="">
				</div>
				<div class="form-group">
					<label for="PApellido">Apellido Parterno</label>
					<input type="text" name="ApellidoPaterno" class='users form-control' value="<?= $apod->ApellidoPaterno?>" id="PApellido" required="">
				</div>
				<div class="form-group">
					<label for="SApellido">Apellido Materno</label>
					<input type="text" name="ApellidoMaterno" class='users form-control' value="<?= $apod->ApellidoMaterno?>" id="SApellido" required="">
				</div>
				<div class="form-group">
					<label for="Correo">Correo Electronico</label>
					<input type="email" name="Correo" class='users form-control' id="Correo" value="<?= $apod->Correo?>" required="">
				</div>
				<div class="form-group">
					<label for="Telefono">Telefono</label>
					<input type="number" name="Telefono" class='users form-control' id="Telefono" value="<?=$apod->Telefono?>" required="">
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg1 btn-block">Guardar</button>
				</div>
				<div class="form-group">
					<a href="<?=base_url()?>Apoderado/Listar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
				</div>
				<?php }?>
				<div "form-group">
	    			<p id="Respuesta"></p>
	    		</div>
			</form>
		</div>
	</div>
	<?php }?>