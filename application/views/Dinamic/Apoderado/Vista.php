<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else{ ?>
	<div class="courses_box1">
	   <div class="container">	
		    <div class="login">
		    	<?php
            	foreach($apoderado as $apod)
            	{ ?>
		    	<p class="lead">Alumno: <?= $apod->Nombre."  ".$apod->ApellidoPaterno."  ".$apod->ApellidoMaterno?></p>
				<div class="form-group">
				    	<label for="usuario">Codigo</label>
					    <input type="text" name="usuario" id="usuario" class='users form-control' value="<?= $apod->Dni?>">
				</div>
			 	<div class="form-group">
			    	<label for="PNombre">Primer Nombre</label>
				    <input type="text" name="PrimerNombre" class='users form-control' value="<?= $apod->Nombre?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<label for="PApellido">Apellido Parterno</label>
				    <input type="text" name="ApellidoPaterno" class='users form-control' value="<?= $apod->ApellidoPaterno?>" disabled='off' id="PApellido">
			    </div>
			    <div class="form-group">
			    	<label for="SApellido">Apellido Materno</label>
				    <input type="text" name="ApellidoMaterno" class='users form-control' value="<?= $apod->ApellidoMaterno?>" disabled='off' id="SApellido">
			    </div>
			    <div class="form-group">
			    	<label for="Correo">Correo Electronico</label>
				    <input type="email" name="Correo" class='users form-control' id="Correo" value="<?= $apod->Correo?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<label for="FechaMatricula">Telefono</label>
				    <input type="number" name="Telefono" class='users form-control' id="Telefono" value="<?= $apod->Telefono?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Apoderado/Listar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
			    </div>
			    <?php }?>
		    </div>
	    </div>
	</div>
	<?php }?>