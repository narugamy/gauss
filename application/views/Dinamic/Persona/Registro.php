<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form = array('class' => 'login','id'=>'myformReg','role'=>'form');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">	
	   <?=form_open(base_url().'Persona/Registrar',$form)?>
	    	<p class="lead">Registro de Alumno</p>
		    <div class="form-group">
		    	<label for="PNombre">Primer Nombre</label>
			    <input type="text" name="PrimerNombre" class='users form-control' placeholder='Primer Nombre' value="" id="PNombre">
			    <p id="pn"></p>
		    </div>
		    <div class="form-group">
		   		<label for="SNombre">Segundo Nombre</label>
			    <input type="text" name="SegundoNombre" class='users form-control' placeholder='Segundo Nombre' value="" id="SNombre">
			    <p id="sn"></p>
		    </div>
		    <div class="form-group">
		    	<label for="PApellido">Apellido Parterno</label>	
			    <input type="text" name="ApellidoPaterno" class='users form-control' placeholder='Apellido Parterno' value="" id="PApellido">
			    <p id="ap"></p>
		    </div>
		    <div class="form-group">
		    	<label for="SApellido">Apellido Materno</label>
			    <input type="text" name="ApellidoMaterno" class='users form-control' placeholder='Apellido Materno' value="" id="SApellido">
			    <p id="am"></p
		    </div>
		    <div class="form-group">
		    	<label for="FechaNacimiento">Fecha de Nacimiento</label>
			    <input type="date" name="FechaNacimiento" class='users form-control' value="" id="FechaNacimiento" placeholder="Contraseña">
			    <p id="fn"></p>
		    </div>
		    <div class="form-group">
			    <input type="hidden" name="Codigo" class='users form-control'>
		    </div>
		    <div class="form-group">
		    	<label for="Clave">Contraseña</label>
			    <input type="password" name="Clave" class='users form-control' placeholder='Contraseña' value"" id="Clave">
			    <p id="pw"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Correo">Correo Electronico</label>
			    <input type="email" name="Correo" class='users form-control' placeholder='Correo Electronico' value="" id="Correo" required>
			    <p id="em"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Sexo">Sexo</label>
		    	<select name="Sexo" id="Sexo" class="users form-control acc" >
		    		<option value="">Seleccione una opcion</option>
		    		<option value="M">Masculino</option>
		    		<option value="F">Femenino</option>
		    	</select>
			    <p id="sx"></p>
		    </div>
		    <div class="form-group">
		    	<label for="FechaMatricula">Fecha de Matricula</label>
			    <input type="date" name="FechaMatricula" class='users form-control'  value="" id="FechaMatricula">
			    <p id="fm"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Ruta">Foto</label>
			    <input type="file" name="Ruta" class='users form-control' placeholder='Img' value="" id="Ruta">
			    <p id="im"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Grado">Grado de Estudio</label>
		    	<select name="Tipo"id="Grado" class="users form-control acc" >
		    		<option value="">Seleccione una opcion</option>
		    		<?php foreach ($tipo as $grado){ ?>
		    		<option value="<?=$grado->idTipoP?>"><?=$grado->Nombre?></option>			
		    		<?php }?>
		    	</select>
			    <p id="gr"></p>
		    </div>
		    <div class="form-group">
		    	<label for="Apoderado">Apoderado</label>
		    	<select name="Apoderado" id="Apoderado" class="users form-control acc" >
		    		<option value="">Seleccione una opcion</option>
		    		<?php foreach ($apoderado as $apod){ ?>
		    		<option value="<?=$apod->idApoderado?>"><?=$apod->Nombre."  ".$apod->ApellidoPaterno?></option>			
		    		<?php }?>
		    	</select>
			    <p id="gr"></p>
		    </div>
		    <div class="form-group">
			    <button type="submit" class="btn btn-primary btn-lg1 btn-block">Registrar</button>
		    </div>
		    <div class="form-group">
		    	<a href="<?=base_url()?>Persona" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
		    </div>
		    <div "form-group">
	    		<p id="Respuesta"></p>
	    	</div>
	    <?=form_close()?>
	   </div>
	</div>
	<?php }?>