<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else{ ?>
	<div class="courses_box1">
	   <div class="container">	
		    <div class="login">
		    	<?php
            	foreach($alumno as $empl)
            	{ ?>
		    	<p class="lead">Alumno: <?= $empl->PrimerNombre."  ".$empl->ApellidoPaterno."  ".$empl->ApellidoMaterno?></p>
				<div class="form-group">
				    	<label for="usuario">Codigo</label>
					    <input type="text" name="usuario" id="usuario" class='users form-control' value="<?= $empl->Codigo?>" disabled='off'>
				</div>
			 	<div class="form-group">
			    	<label for="PNombre">Primer Nombre</label>
				    <input type="text" name="PrimerNombre" class='users form-control' value="<?= $empl->PrimerNombre?>" disabled='off'>
			    </div>
			    <div class="form-group">
			   		<label for="SNombre">Segundo Nombre</label>
				    <input type="text" name="SegundoNombre" class='users form-control' value="<?= $empl->SegundoNombre?>" disabled='off'id="SNombre">
			    </div>
			    <div class="form-group">
			    	<label for="PApellido">Apellido Parterno</label>	
				    <input type="text" name="ApellidoPaterno" class='users form-control' value="<?= $empl->ApellidoPaterno?>" disabled='off' id="PApellido">
			    </div>
			    <div class="form-group">
			    	<label for="SApellido">Apellido Materno</label>
				    <input type="text" name="ApellidoMaterno" class='users form-control' value="<?= $empl->ApellidoMaterno?>" disabled='off' id="SApellido">
			    </div>
			    <div class="form-group">
			    	<label for="FechaNacimiento">Fecha de Nacimiento</label>
				    <input type="date" name="FechaNacimiento" class='users form-control' id="FechaNacimiento" value="<?= $empl->FechaNacimiento?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<label for="Correo">Correo Electronico</label>
				    <input type="email" name="Correo" class='users form-control' id="Correo" value="<?= $empl->Correo?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<label for="FechaMatricula">Fecha de Matricula</label>
				    <input type="date" name="FechaMatricula" class='users form-control' id="FechaMatricula" value="<?= $empl->FechaMatricula?>" disabled='off'>
			    </div>
			    <div class="form-group">
			    	<a href="<?=base_url()?>Persona/Listar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
			    </div>
			    <?php }?>
		    </div>
	    </div>
	</div>
	<?php }?>