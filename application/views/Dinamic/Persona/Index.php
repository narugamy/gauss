<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if($this->session->userdata('Perfil')!='admin')
	  { 
	  	redirect(base_url());?>	
<?php }else
	  {?>
	<div class="courses_box1">
	   <div class="container">	
	   		<div class="login">
                <h3>Listado de Opciones</h3>
                <div class="form-group">
                    <a href="<?=base_url()?>Persona/Registro" class="btn btn-primary btn-lg1 btn-block boton" >Registrar Nuevo Alumno</a>
                </div>
                <div class="form-group">
                    <a href="<?=base_url()?>Persona/Buscar" class="btn btn-primary btn-lg1 btn-block boton" >Buscar Alumno</a>
                </div>
            </div>
	   </div>
	</div>
	<?php }?>