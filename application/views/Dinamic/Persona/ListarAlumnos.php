<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if($this->session->userdata('Perfil')==''){
	redirect(base_url());
}
?>
<?php 
?>
    <!-- //banner -->
	<div class="courses_box1">
		<div class="container">
	   		<div class="login">
	       		<h3>Lista de Alumnos Encontrados</h3>
	            <?php
	            	foreach($alumno as $empl)
	            	{ ?>
	            	<div class="form-group">
		                <a href="<?=base_url()?>Persona/Persona" class="btn btn-success botones" value="<?= $empl->Codigo?>" role="buttohon"><?= $empl->PrimerNombre."  ".$empl->ApellidoPaterno?></a> 
		                <a href="<?=base_url()?>Persona/Editar" class="btn btn-warning botones" value="<?= $empl->Codigo?>" role="buttohon">Editar</a>
		                <?php if($empl->Estado==1){?>
		                <a href="<?=base_url()?>Persona/Eliminar" class="btn btn-danger botones" value="<?= $empl->Codigo?>" role="button">Eliminar</a>
		                <?php }else{?>
						<a href="<?=base_url()?>Persona/Habilitar" class="btn btn-primary botones" value="<?= $empl->Codigo?>" role="button">Habilitar</a>
		                <?php }?>
		            </div>
	          <?php }?>
	            <hr>
	            <a href="<?=base_url()?>persona/Buscar/" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
           </div>
	   </div>
	</div>