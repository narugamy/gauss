<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php if(!$this->session->userdata('Perfil')=='admin'){
		redirect(base_url());?>
<?php }else{ ?>
	<div class="courses_box1">
		<div class="container">
			<form action="<?=base_url()?>Persona/Update" method="post" class="login" id="update">
				<?php   foreach($alumno as $empl){ ?>
				<p class="lead">Alumno: <?= $empl->PrimerNombre."  ".$empl->ApellidoPaterno."  ".$empl->ApellidoMaterno?></p>
				<div class="form-group">
					<input type="hidden" name="Codigo" id="usuario" class='users form-control' value="<?= $empl->Codigo?>">
				</div>
				<div class="form-group">
					<input type="hidden" name="valor" id="valor" class='users form-control' value="3">
				</div>
				<div class="form-group">
					<label for="PNombre">Primer Nombre</label>
					<input type="text" name="PrimerNombre" class='users form-control' value="<?= $empl->PrimerNombre?>">
					<p id="pn"></p>
				</div>
				<div class="form-group">
					<label for="SNombre">Segundo Nombre</label>
					<input type="text" name="SegundoNombre" class='users form-control' value="<?= $empl->SegundoNombre?>" id="SNombre">
					<p id="sn"></p>
				</div>
				<div class="form-group">
					<label for="PApellido">Apellido Parterno</label>
					<input type="text" name="ApellidoPaterno" class='users form-control' value="<?= $empl->ApellidoPaterno?>" id="PApellido">
					<p id="ap"></p>
				</div>
				<div class="form-group">
					<label for="SApellido">Apellido Materno</label>
					<input type="text" name="ApellidoMaterno" class='users form-control' value="<?= $empl->ApellidoMaterno?>" id="SApellido">
					<p id="am"></p>
				</div>
				<div class="form-group">
					<label for="FechaNacimiento">Fecha de Nacimiento</label>
					<input type="date" name="FechaNacimiento" class='users form-control' id="FechaNacimiento" value="<?= $empl->FechaNacimiento?>">
					<p id="fn"></p>
				</div>
				<div class="form-group">
					<label for="FechaMatricula">Fecha de Matricula</label>
					<input type="date" name="FechaMatricula" class='users form-control' id="FechaMatricula" value="<?=$empl->FechaMatricula?>">
					<p id="fm"></p>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-lg1 btn-block">Guardar</button>
				</div>
				<div class="form-group">
					<a href="<?=base_url()?>Persona/Listar" class="btn btn-primary btn-lg1 btn-block boton">Atras</a>
				</div>
				<?php }?>
				<div "form-group">
	    			<p id="Respuesta"></p>
	    		</div>
			</form>
		</div>
	</div>
	<?php }?>