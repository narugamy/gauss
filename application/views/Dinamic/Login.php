<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form = array('class' => 'login','id'=>'myform','role'=>'form');
?>
<?php if(!$this->session->userdata('Perfil')=='')
	  { 
	  	redirect(base_url());?>	
<?php }else{ ?>
	<div class="courses_box1">
	   <div class="container">	
	   <?=form_open(base_url().'Login/Nuevo',$form)?>
	    	<p class="lead">Bienvenido</p>
		    <div class="form-group">
		    	<label for="usuario">Codigo</label>
			    <input type="text" name="usuario" id="usuario" class='users form-control' placeholder='Nick' value="">
			    <p id="us"></p>
		    </div>
		    <div class="form-group">
		    	<label for="password">Clave</label>
		    	<input type="password" name='password' id="password" class='password form-control' placeholder='Password' value=''>
		    	<p id="pw"></p>
		    </div>
		    <div class="form-group">
		    	<p id="respuesta"></p>
		    </div>
		    <div class="form-group">
		    	<button type="submit" class="btn btn-primary btn-lg1 btn-block">Login</button>
		    </div>
	    <?=form_close()?>
	   </div>
	</div>
	<?php }?>