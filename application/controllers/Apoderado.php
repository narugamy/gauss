<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apoderado extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Modelnivel');
		$this->load->model('Modelapoderado');
		$this->load->model('Modelpersona');
	}

	public function Apoderado(){
		$id=$this->input->post('valor');
		if($id==NULL or !is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Alumno');
			$lista=$this->Modelapoderado->get($id,null,2);
			$data=array('visual'=>'Dinamic/Apoderado/Vista.php','apoderado'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	function Buscar($error=null){
		$dato=array('Titulo_Sitio'=>'Buscar Apoderado');
		$data=array('visual'=>'Dinamic/Apoderado/Buscar.php');
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Correcto(){
		$dato=$this->input->post('Dato');
		$this->Buscar($dato);
	}

	function Editar(){
		$id=$this->input->post('valor');
		if($id==NULL or !is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Apoderado');
			$lista=$this->Modelapoderado->get($id);
			$data=array('visual'=>'Dinamic/Apoderado/Update.php','apoderado'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	public function Eliminar(){
		$id=$this->input->post('valor');
		$lista=$this->Modelapoderado->estado($id,2);
		if($this->input->post('valor')){
			$this->Buscar("Eliminacion Exitosa");
		}else{
			$this->Buscar("Error al Eliminar");
		}
	}

	public function Habilitar(){
		$id=$this->input->post('valor');
		$lista=$this->Modelapoderado->estado($id,1);
		if($this->input->post('valor')){
			$this->Buscar("Habilitacion Exitosa");
		}else{
			$this->Buscar("Error al Actualizar");
		}
	}

	public function index(){
		$dato=array('Titulo_Sitio'=>'Panel Apoderado');
		$data=array('visual'=>'Dinamic/Apoderado/Index.php','url'=>base_url());
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}

	}

	function Listar(){
		$dato=array('Titulo_Sitio'=>'Lista de Apoderado');
		$cod=$this->input->post('Codigo');
		$lista=$this->Modelapoderado->get($cod);
		$data=array('visual'=>'Dinamic/Apoderado/ListarApoderados.php','apoderado'=>$lista);
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	public function Registrar(){
		$this->Validar();
		$error=array();
		$dato=$this->input->post();
		if($dato['Dni']){
			if($this->form_validation->run() == false){
				$error['Correo']=form_error('Correo');
				$error['Nombre']=form_error('Nombre');
				$error['ApellidoPaterno']=form_error('ApellidoPaterno');
				$error['ApellidoMaterno']=form_error('ApellidoMaterno');
				$error['Dni']=form_error('Dni');
				$error['Telefono']=form_error('Telefono');
				$error['exito']=false;
			}else {
				if(empty($error['Correo']) && empty($errror['Nombre']) && empty($error['ApellidoPaterno']) && empty($error['ApellidoPaterno']) && empty($error['Dni']) &&
					empty($error['Telefono'])){
					$ver=$this->Modelapoderado->verificarCorreo($dato['Correo']);
					if($ver==true)
					{
						$error['exito']=false;
						$error['Correo']="El correo ya esta en uso";
					}else{
						unset($dato['valor']);
						$this->Modelapoderado->registro($dato);
						$error['exito']=true;
						$error['url']=base_url()."Apoderado/Correcto";
						$error['html']="Registro Exitoso";
					}
				}
			}
			echo json_encode($error);
		}else{
			$this->Error();
		}
	}

	public function Registro($error=null){
		$dato=array('Titulo_Sitio'=>'Registro Apoderado');
		$data=array('visual'=>'Dinamic/Apoderado/Registro.php','url'=>base_url());
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Update(){

		if($this->input->post('Dni')){
			$id=$this->input->post('Correo');
			$array=$this->input->post();
			$cod=$array['Dni'];
			$tel=$array['Telefono'];
			unset($array['Dni'],$array['valor']);
			if($this->Modelapoderado->verificarCorreo($id)==true){
				unset($array['Correo']);
			}
			if($this->Modelapoderado->verificarTelefono($tel)==true){
				unset($array['Telefono']);
			}
			$lista=$this->Modelapoderado->edit($array,$cod);
			$data['exito']=true;
			$data['url']=base_url()."Persona/Correcto";
			$data['html']="Actualizacion Exitosa";
			echo json_encode($data);
		}else{
			$this->Error();
		}
	}

	function Error(){
		$data['visual']='errors/html/NoExiste.php';
		$dato=array('Titulo_Sitio'=>'Modulo Error Apoderado');
		$this->Visual($dato,$data);
	}

	function Validar(){
		$this->form_validation->set_rules('Nombre', 'Primer Nombre', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ApellidoPaterno', 'Apellido Paterno', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ApellidoMaterno', 'Apellido Materno', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Dni', 'Dni', 'trim|required|min_length[8]|max_length[8]|xss_clean|is_unique[apoderados.Dni]');
		$this->form_validation->set_rules('Correo', 'Correo', 'trim|required|xss_clean|valid_email|is_unique[apoderados.Correo]');
		$this->form_validation->set_rules('Telefono', 'Telefono', 'trim|required|min_length[7]|max_length[8]|xss_clean|is_unique[apoderados.Telefono]');
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		$this->form_validation->set_message('min_length', 'El  %s tiene menos caracteres de lo devido');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres de lo devido');
		$this->form_validation->set_message('valid_email', 'El  %s No es un Email');
		
	}

	function Visual($dato,$data){
		switch ($this->session->userdata('Perfil')) {
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

}