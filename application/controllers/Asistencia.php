<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asistencia extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$dato=array('Titulo_Sitio'=>'Panel Apoderado');
		$data=array('visual'=>'Dinamic/Asistencia/Asistencia.php','url'=>base_url());
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	public function Registrar(){
		$error=array();
		$this->Validar();
		if($this->form_validation->run() == false){
			$error['Respuesta']=form_error('Codigo');
			$error['Respuesta2']=form_error('Contraseña');
		}else{
			$error['Respuesta']="Succes";
		}
		echo json_encode($error);
	}

	function visual($dato,$data){
		switch ($this->session->userdata('Perfil')){
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

	public function Validar(){
		$this->form_validation->set_rules('Codigo', 'Codigo', 'trim|required|xss_clean');
        $this->form_validation->set_rules('Contraseña', 'Contraseña', 'trim|required|xss_clean');
        $this->form_validation->set_message('required', 'El  %s es requerido');
	}
}
