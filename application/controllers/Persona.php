<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persona extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Modelapoderado');
		$this->load->model('Modelnivel');
		$this->load->model('Modelpersona');
		$this->load->model('Modeltipopersona');
	}

	function Buscar($error=null){
		$dato=array('Titulo_Sitio'=>'Buscar Alumno');
		$data=array('visual'=>'Dinamic/Persona/Buscar.php');
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->visual($dato,$data);
		}
	}

	public function Correcto(){
		$dato=$this->input->post('Dato');
		$this->Buscar($dato);
	}

	function Editar(){
		$id=$this->input->post('valor');
		if($id==NULL or is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Alumno');
			$lista=$this->Modelpersona->get($id,null,2);
			$data=array('visual'=>'Dinamic/Persona/Update.php','alumno'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	public function Eliminar(){
		$id=$this->input->post('valor');
		$lista=$this->Modelpersona->estado($id,2);
		if($this->input->post('valor')){
			$this->Buscar("Eliminacion Exitosa");
		}else{
			$this->Buscar("Error al Eliminar");
		}
	}

	function Error(){
		$data['visual']='errors/html/NoExiste.php';
		$dato=array('Titulo_Sitio'=>'Modulo Error Alumno');
		$this->Visual($dato,$data);
	}

	public function Habilitar(){
		$id=$this->input->post('valor');
		$lista=$this->Modelpersona->estado($id,1);
		if($this->input->post('valor')){
			$this->Buscar("Habilitacion Exitosa");
		}else{
			$this->Buscar("Error al Actualizar");
		}
	}

	public function index(){
		$dato=array('Titulo_Sitio'=>'Panel Persona');
		$data=array('visual'=>'Dinamic/Persona/Index.php','url'=>base_url());
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Listar(){
		$dato=array('Titulo_Sitio'=>'Lista de Alumno');
		$cod=$this->input->post('Codigo');
		$lista=$this->Modelpersona->get($cod,null,2);
		$data=array('visual'=>'Dinamic/Persona/ListarAlumnos.php','alumno'=>$lista);
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	public function Persona(){
		$id=$this->input->post('valor');
		if($id==NULL or is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Alumno');
			$lista=$this->Modelpersona->get($id,null,2);
			$data=array('visual'=>'Dinamic/Persona/Vista.php','alumno'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	public function Registrar(){
		$this->Validar(1);
		$error=array();
		$dato=$this->input->post();
		if($dato['Correo']){
			if($this->form_validation->run() == false){
				$error['Correo']=form_error('Correo');
				$error['PrimerNombre']=form_error('PrimerNombre');
				$error['SegundoNombre']=form_error('SegundoNombre');
				$error['ApellidoPaterno']=form_error('ApellidoPaterno');
				$error['ApellidoMaterno']=form_error('ApellidoMaterno');
				$error['FechaNacimiento']=form_error('FechaNacimiento');
				$error['Clave']=form_error('Clave');
				$error['Sexo']=form_error('Sexo');
				$error['FechaMatricula']=form_error('FechaMatricula');
				$error['Tipo']=form_error('Tipo');
				$error['exito']=false;
			}else{
				if(empty($error['Correo']) && empty($error['PrimerNombre'])&&empty($error['SegundoNombre'])&&empty($error['ApellidoPaterno'])&&empty($error['FechaNacimiento'])&&
					empty($error['Clave'])&&empty($error['Sexo'])&&empty($error['FechaMatricula'])&&empty($error['Tipo'])){
					unset($dato['valor']);
						if(empty($dato['Apoderado'])){
							$dato['Apoderado']=null;
						}
					$this->Modelpersona->registro($dato);
					$error['exito']=true;
					$error['url']=base_url()."Persona/Correcto";
					$error['html']="Registro Exitoso";
				}
			}
			echo json_encode($error);
		}else{
			$this->Error();
		}
	}

	public function Registro($error=null){
		$dato=array('Titulo_Sitio'=>'Registro Persona');
		$nivel=$this->Modelnivel->get_todos();
		$tipo=$this->Modeltipopersona->get_todos();
		$apoderado=$this->Modelapoderado->get(null);
		$data=array('visual'=>'Dinamic/Persona/Registro.php','url'=>base_url(),'tipo'=>$tipo,'apoderado'=>$apoderado);
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Update(){
		$this->Validar(null);
		$error=array();
		$array=$this->input->post();
		if($array['Codigo']){
			if($this->form_validation->run() == false){
				$error['SegundoNombre']=form_error('SegundoNombre');
				$error['ApellidoPaterno']=form_error('ApellidoPaterno');
				$error['ApellidoMaterno']=form_error('ApellidoMaterno');
				$error['FechaNacimiento']=form_error('FechaNacimiento');
				$error['FechaMatricula']=form_error('FechaMatricula');
				$error['exito']=false;
			}else{
				$cod=$array['Codigo'];
				unset($array['Codigo'],$array['valor']);
				$lista=$this->Modelpersona->edit($array,$cod);
				$error['exito']=true;
				$error['url']=base_url()."Persona/Correcto";
				$error['html']="Actualizacion Exitosa";
			}
			echo json_encode($error);
		}else{
			$this->Error();
		}
	}

	function Validar($var){
		$this->form_validation->set_rules('PrimerNombre', 'Primer Nombre', 'trim|required|xss_clean');
		$this->form_validation->set_rules('SegundoNombre', 'Segundo Nombre', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ApellidoPaterno', 'Apellido Paterno', 'trim|required|xss_clean');
		$this->form_validation->set_rules('ApellidoMaterno', 'Apellido Materno', 'trim|required|xss_clean');
		$this->form_validation->set_rules('FechaNacimiento', 'Fecha de Nacimiento', 'trim|required|xss_clean');
		$this->form_validation->set_rules('FechaMatricula', 'Fecha de Matricula', 'trim|required|xss_clean');
		if($var==1){
			$this->form_validation->set_rules('Correo', 'Correo', 'trim|required|xss_clean|is_unique[personas.Correo]');
			$this->form_validation->set_rules('Tipo', 'Tipo', 'trim|required|xss_clean');
			$this->form_validation->set_rules('Clave', 'Clave', 'trim|required|min_length[8]|max_length[120]|xss_clean');
			$this->form_validation->set_rules('Sexo', 'Sexo', 'trim|required|xss_clean');
			$this->form_validation->set_message('is_unique', 'El  %s ya esta en uso');
		}
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('min_length', 'El  %s tiene menos caracteres');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres');
	}

	function Visual($dato,$data){
		switch ($this->session->userdata('Perfil')) {
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

	}