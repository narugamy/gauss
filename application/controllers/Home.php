<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{

	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		switch($this->session->userdata('Perfil')){
			case '':
			$dato=array('Titulo_Sitio'=>'Bienvenidos al SystemProyect');
			$data=array('visual'=>'Index.php',);
			$this->visual($dato,$data);
			break;
			case 'admin':
				$dato=array('Titulo_Sitio'=>'Bienvenidos al SystemProyect');
				$data=array('visual'=>'Index.php',);
				$this->visual($dato,$data);
			break;
			default:
				redirect(base_url().'admin');
			break;		
		}
	}

	function visual($dato,$data){
		switch ($this->session->userdata('Perfil')){
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

}
