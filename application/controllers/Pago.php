<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pago extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Modelpersona');
		$this->load->model('Modelpago');
		$this->load->helper('date');
	}

	function Buscar($error=null){
		$dato=array('Titulo_Sitio'=>'Buscar Apoderado');
		$data=array('visual'=>'Dinamic/Pago/Buscar.php');
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Correcto(){
		$dato=$this->input->post('Dato');
		$this->Buscar($dato);
	}

	function Editar(){
		$id=$this->input->post('valor');
		if($id==NULL or !is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Apoderado');
			$lista=$this->Modelpago->get($id,2);
			$data=array('visual'=>'Dinamic/Pago/Update.php','Pago'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	function Error(){
		$data['visual']='errors/html/NoExiste.php';
		$dato=array('Titulo_Sitio'=>'Modulo Error Apoderado');
		$this->Visual($dato,$data);
	}

	public function index(){
		$dato=array('Titulo_Sitio'=>'Panel Pago');
		$data=array('visual'=>'Dinamic/Pago/Index.php','url'=>base_url());
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Listar(){
		$dato=array('Titulo_Sitio'=>'Lista de Apoderado');
		$cod=$this->input->post('Codigo');
		$lista=$this->Modelpersona->get($cod,null,2);
		foreach ($lista as $pag) {
			$id=$pag->idPersona;
		}
		$pago=$this->Modelpago->get($id,1);
		$data=array('visual'=>'Dinamic/Pago/ListarPagos.php','Pago'=>$pago,'Alumno'=>$lista);
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	public function Pago(){
		$id=$this->input->post('valor');
		if($id==NULL or !is_numeric($id)){
			$this->Error();
		}else{
			$dato=array('Titulo_Sitio'=>'Vista Alumno');
			$lista=$this->Modelpago->get($id,2);
			$data=array('visual'=>'Dinamic/Pago/Vista.php','Pago'=>$lista);
			if($this->input->post('valor')){
				$this->load->view($data['visual'],$data);
			}else{
				$this->Visual($dato,$data);
			}
		}
	}

	public function Registrar(){
		$this->Validar();
		$error=array();
		$dato=$this->input->post();
		if($dato['valor']){
			if($this->form_validation->run() == false){
				$error['Persona']=form_error('Persona');
				$error['Cantidad']=form_error('Cantidad');
				$error['NumeroRecibo']=form_error('NumeroRecibo');
				$error['Fecha']=form_error('Fecha');
				$error['exito']=false;
			}else{
				if(empty($error['Persona']) && empty($errror['Cantidad']) && empty($error['NumeroRecibo']) && empty($error['Fecha'])){
					unset($dato['valor']);
					$this->Modelpago->registro($dato);
					$error['exito']=true;
					$error['url']=base_url()."Pago/Correcto";
					$error['html']="Registro Exitoso";
				}
			}
			echo json_encode($error);
		}else{
			$this->Error();
		}
	}

	public function Registro($error=null){
		$dato=array('Titulo_Sitio'=>'Registro Apoderado');
		$alumno=$this->Modelpersona->get_todos();
		$data=array('visual'=>'Dinamic/Pago/Registro.php','alumno'=>$alumno);
		if($error!=null){
			$data['error']=$error;
		}
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->Visual($dato,$data);
		}
	}

	function Update(){
		if($this->input->post('idPago')){
			$id=$this->input->post('idPago');
			$array=$this->input->post();
			unset($array['idPago'],$array['valor']);
			$lista=$this->Modelpago->edit($array,$id);
			$error['exito']=true;
			$error['url']=base_url()."Pago/Correcto";
			$error['html']="Actualizacion Exitosa";
			echo json_encode($error);
		}else{
			$this->Error();
		}
	}

	function Validar(){
		$this->form_validation->set_rules('Persona', 'Persona', 'trim|required|xss_clean');
		$this->form_validation->set_rules('Cantidad', 'Cantidad', 'trim|required|min_length[1]|max_length[3]|xss_clean');
		$this->form_validation->set_rules('NumeroRecibo', 'Numero de Recibo', 'trim|required|min_length[8]|max_length[8]|xss_clean');
		$this->form_validation->set_rules('Fecha', 'Fecha', 'trim|required|xss_clean');
		$this->form_validation->set_message('required', 'El  %s es requerido');
		$this->form_validation->set_message('min_length', 'El  %s tiene menos caracteres');
		$this->form_validation->set_message('max_length', 'El  %s tiene mas caracteres');
	}

	function Visual($dato,$data){
		switch ($this->session->userdata('Perfil')) {
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

}