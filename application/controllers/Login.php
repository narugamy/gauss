<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Modelpersona');
		$this->load->model('Modelnivel');
		$this->load->model('Modeltipopersona');
	}

	public function index(){
		$dato=array('Titulo_Sitio'=>'Login KFGauss');
		$data=array('visual'=>'Dinamic/Login.php');
		if($this->input->post('valor')){
			$this->load->view($data['visual'],$data);
		}else{
			$this->visual($dato,$data);
		}
	}

	public function cerrar_sesion(){
		if($this->session->userdata('Perfil')!=''){
			$this->session->sess_destroy();
			$valor['url']=base_url();
		}else{
			$valor['url']=base_url();
		}
		echo json_encode($valor);
	}

	public function nuevo(){
		if($this->session->userdata('Perfil')!=''){
			redirect(base_url());
		}else{
			$username = $this->input->post('usuario');
			$password = $this->input->post('password');
			$error=array();
			$check_user=array();
			if(empty($username)){
				$error['usuario']="Se require un usuario";
			}
			if(empty($password)){
				$error['password']="Se require una clave";
			}
			$check_user['exito'] = $this->Modelpersona->verificar($username,$password);
			if($check_user['exito']== false){
				$check_user['errores']=$error;
			}else{
				$dato=$this->Modelpersona->get($username,$password,1);
				$nivel=$this->Modelnivel->get($dato->Nivel);
				$Tipo=$this->Modeltipopersona->get($dato->Tipo);
				$data = array(
					'is_logued_in' 	=> 		TRUE,
					'Perfil'		=>		$nivel->Nombre,
					'Nombre'		=>		$dato->PrimerNombre,
					'Apellidos'		=>		$dato->ApellidoPaterno,
					'Codigo'	 	=> 		$dato->Codigo,
					'Tipo' 			=> 		$Tipo->Nombre,
					'Img'			=>		$dato->Ruta
				);
				$this->session->set_userdata($data);
				$check_user['url']=base_url();
			}
			echo json_encode($check_user);
		}
	}

	function visual($dato,$data){
		switch ($this->session->userdata('Perfil')){
			case 'admin':
				$url='Dinamic/Admin';
				break;
			case 'alumno':
				$url='Dinamic/Alumno';
				break;
			default:
				$url='Layout';
				break;
		}
		$this->load->view("$url/header",$dato);
		$this->load->view($data['visual'],$data);
		$this->load->view("$url/footer");
	}

}