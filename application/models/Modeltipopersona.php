<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Modeltipopersona extends CI_Model {

    function get_todos(){
		$Sql=$this->db->get('Tipopersona');
		return $Sql->result();
	}

	public function get($codigo){
		$this->db->where('idTipoP',$codigo);
		$query = $this->db->get('tipopersona');
		return $query->row();
	}
	
}