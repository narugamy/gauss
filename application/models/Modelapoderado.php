<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modelapoderado extends CI_Model{


	public function edit($array,$id){
		$this->db->where('Dni',$id);
		$this->db->update('apoderados',$array);
	}

	public function estado($id,$cod){
		$dato['Estado']=$cod;
		$this->db->where('Dni',$id);
		$this->db->update('apoderados',$dato);
	}

	public function get($user){
		if(!empty($user)){
			$this->db->where('Dni',$user);
			$this->db->or_where('ApellidoPaterno',$user);
			$query = $this->db->get('apoderados');
			return $query->result();
		}else{
			$query = $this->db->get('apoderados');
			return $query->result();
		}
	}

	public function registro($array){
		$this->db->insert('apoderados',$array);
	}

	public function verificar($user,$pass){
		$this->db->where('Codigo',$user);
		$this->db->where('Clave',$pass);
		$query = $this->db->get('personas');
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

	public function verificarCorreo($email){
		$this->db->where('Correo',$email);
		$query = $this->db->get('apoderados');
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

	public function verificarTelefono($telefono){
		$this->db->where('Telefono',$telefono);
		$query = $this->db->get('apoderados');
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

}