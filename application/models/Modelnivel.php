<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Modelnivel extends CI_Model{

    function get_todos(){
		$Sql=$this->db->get('nivel');
		return $Sql->result();
	}

	public function get($codigo){
		$this->db->where('idNivel',$codigo);
		$query = $this->db->get('nivel');
		return $query->row();
	}

}