<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modelpersona extends CI_Model{

	public function edit($array,$id){
		$this->db->where('Codigo',$id);
		$this->db->update('personas',$array);
	}

	public function estado($id,$cod){
		$dato['Estado']=$cod;
		$this->db->where('Codigo',$id);
		$this->db->update('personas',$dato);
	}

	public function get($user,$pass,$valor){
		if($valor==1){
			$this->db->where('Codigo',$user);
			$this->db->where('Clave',$pass);
			$query = $this->db->get('personas');
			return $query->row();
		}else if($valor==2){
			if(!empty($user)){
				$this->db->where('Codigo',$user);
				$this->db->or_where('ApellidoPaterno',$user);
				$query = $this->db->get('personas');
				return $query->result();
			}else{
				$query = $this->db->get('personas');
				return $query->result();
			}
		}
	}

    function get_todos(){
		$Sql=$this->db->get('personas');
		return $Sql->result();
	}

	public function registro($array){
		$this->db->insert('personas',$array);
	}

	public function verificar($user,$pass){
		$this->db->where('Codigo',$user);
		$this->db->where('Clave',$pass);
		$query = $this->db->get('personas');
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

	public function verificarCorreo($email){
		$this->db->where('Correo',$email);
		$query = $this->db->get('personas');
		if($query->num_rows() == 1){
			return true;
		}else{
			return false;
		}
	}

}