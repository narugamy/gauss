<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Modelpago extends CI_Model{

	public function edit($array,$id){
		$this->db->where('idPago',$id);
		$this->db->update('pagos',$array);
	}

    public function registro($array){
		$this->db->insert('pagos',$array);
	}
	public function get($codigo,$dato){
		if($dato==1){
			$this->db->where('Persona',$codigo);
		}else{
			$this->db->where('idPago',$codigo);
		}
		$query = $this->db->get('pagos');
		return $query->result();
	}

}