function relog(){
	    var relog = new Date();
	    var hora = relog.getHours();
	    var minutos = relog.getMinutes();
	    var segundos = relog.getSeconds();
	    var tipo;
	    if(minutos<10)
	    	minutos="0"+minutos;
	    else{
	    	minutos=""+minutos;
	    }
	    if(minutos<10){
	    	segundos="0"+segundos;
	    }else{
	    	segundos=""+segundos;
	    }
	    if(hora>12){
	    	tipo="AM"
	    	hora-=12;
	    }else{
	    	tipo="PM"
	    }
	    if(hora==0){
	    	hora=12;
	    }else{
	    	hora=hora;
	    }

	    var Tiempo = hora + ":" + minutos + ":" + segundos + " " + tipo;
	    $("#clock").html(Tiempo);
	}

$(document).on('ready',function(){

	var base_url='http://localhost/Gauss/'
	/* Arcoordion */
	var heading=$('.panel-heading');

	$('.panel-title > a').on('click',function(){
		$(heading).removeClass('activo');
		$(this).parent().parent().addClass('activo');
	});

	var nav=$('.navbar-fixed-top');

	$(window).scroll(function(){
		var scroll=$(window).scrollTop();
		if(scroll >=300){
			nav.addClass('active');
		}else{
			nav.removeClass('active');
		}
	});
	/* ---- Banner ---- */
    $(".dropdown").hover(
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');
        }
    );
	/* ---- Countdown timer ---- */

	$('#counter').countdown({
		timestamp : (new Date()).getTime() + 08*10*60*60*1000
	});


	/* ---- Animations ---- */

	$('#links a').hover(
		function(){ $(this).animate({ left: 3 }, 'fast'); },
		function(){ $(this).animate({ left: 0 }, 'fast'); }
	);

	$('footer a').hover(
		function(){ $(this).animate({ top: 3 }, 'fast'); },
		function(){ $(this).animate({ top: 0 }, 'fast'); }
	);

	/* ---- Ajax Login ---- */
	$('#contenido').on('submit','#myform',function(e){
		var web=$(this).attr('action');
		var datos=$(this).serialize();
		e.preventDefault();
		$.post(web,datos,function(dato)
		{
			console.log(dato);
			if(dato.exito)
			{
				window.location.href=dato.url;
			}else{
				validar('us',dato.errores.usuario);
				validar('pw',dato.errores.password);
				if(!dato.errores.usuario &&  !dato.errores.password)
					$('#respuesta').text("Verifique que los datos sean los correctos");
				else
					$('#respuesta').text("");
			}
		},'json');
	});

	//Cerrar Cession

	$('#salir').on('click','',function(e){
		var web=$(this).attr('href');
		e.preventDefault();
		$.post(web,{},function(dato)
		{
			window.location.href=dato.url;
		},"json");
	});

	//url panel Alumno

	$('#alum').on('click',function(event){
		var web=$(this).attr('href');
		var datos={
			'valor' : '3'
		};
		event.preventDefault();
		$.post(web,datos,null,'html')
			.done(function(dato){
				$('#contenido').html(dato);
				history.pushState({}, 'Login KFGauss', web);
				initControls();
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
	});

	function validar(id,valor)
	{
		if(valor){
			$('#'+id).html(valor);
			$('#'+id).addClass("alert alert-danger");
		}else{
			$('#'+id).text("");
			$('#'+id).removeClass();
		}
	}

	function evento(clase,contenido,valor){
		$('#'+contenido).on('click','.'+clase,function(event){
		var web=$(this).attr('href');
		var datos={
			'valor' : valor
		};
		event.preventDefault();
		$.post(web,datos,null,'html')
			.done(function(dato){
				$('#contenido').html(dato);
				history.pushState({}, 'Busqueda Alumno index', web);
				initControls();
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
		});
	}

	function reiniciar(formulario){
		$('#'+formulario).each(function(){
		this.reset();
		});
	}

	setInterval('relog()', 1000);

	function Register(clase,contenido,vento,formulario,funcion){
		$('#'+contenido).on(vento,clase,function(e){
		var web=$(this).attr('action');
		var datos=$(this).serialize();
		e.preventDefault();
		$.post(web,datos,null,'json')
			.done(function(dato)
			{
				if(dato.exito){
					var datos={
						'valor' : '3',
						'Dato': dato.html
					};
					history.pushState({}, 'Busqueda Alumno index', dato.url);
					initControls();
					$.post(dato.url,datos,null,'html')
						.done(function(dato){
							$('#'+contenido).html(dato);
							initControls();
						})
						.fail(function(){
							alert("Error en el Proceso");
						});
				}else{
					if(funcion=='Registro'){
						Registro(formulario,dato);
					}else{
						Pago(formulario,dato);
					}
				}
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
		});
	}

	function Registro(id,dato){
		validar('ap',dato.ApellidoPaterno);
		validar('am',dato.ApellidoMaterno);
		validar('em',dato.Correo);
		if(id=='myformRegA'){
			validar('no',dato.Nombre);
			validar('dn',dato.Dni);
			validar('te',dato.Telefono);
		}else{
			validar('fn',dato.FechaNacimiento);
			validar('pn',dato.PrimerNombre);
			validar('sn',dato.SegundoNombre);
			validar('pw',dato.Clave);
			validar('sx',dato.Sexo);
			validar('fm',dato.FechaMatricula);
			validar('gr',dato.Tipo);
		}
	}

	function Pago(id,dato){
		validar('co',dato.Persona);
		validar('nr',dato.NumeroRecibo);
		validar('ca',dato.Cantidad);
		validar('fe',dato.Fecha);
	}

	//Update
	Register('#myformRegA','contenido', 'submit', 'myformRegA','Registro');
	//Registro Apoderado
	Register('#update','contenido', 'submit', 'update','Registro');
	//Pago
	Register('#Pago','contenido', 'submit', 'Pago','Pago');
	//Registro  Alumnp
	Register('#myformReg','contenido', 'submit', 'myformReg','Registro');

	//Mostar Alumnos
		$('#contenido').on('click','#buscaral',function(event){
		var web=$(this).attr('href');
		var codigo=$('#usuario').val();
		var datos={
			'valor' : '3',
			'Codigo': codigo
		};
		event.preventDefault();
		$.post(web,datos,null,'html')
			.done(function(dato){
				$('#contenido').html(dato);
				history.pushState({}, 'Busqueda Alumno index', web);
				initControls();
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
		});

	//Atraz
	evento('boton','contenido',3);
	evento('poli','bs-example-navbar-collapse-1',3);
	evento('urllogin','bs-example-navbar-collapse-1',3);
	$('#contenido').on('click','.botones',function(event){
		var web=$(this).attr('href');
		var valor=$(this).attr('value');
		var datos={
			'valor' : valor
		};
		event.preventDefault();
		$.post(web,datos,null,'html')
			.done(function(dato){
				$('#contenido').html(dato);
				history.pushState({}, 'Busqueda Alumno index', web);
				initControls();
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
	});

	$('#contenido').on('submit','#Asistencia',function(event){
		var web=$(this).attr('action');
		var datos=$(this).serialize();
		event.preventDefault();
		$.post(web,datos,null,'json')
			.done(function(dato){
				$('#Respuesta').html(dato.Respuesta);
				$('#Respuesta2').html(dato.Respuesta2);
				initControls();
				//history.pushState({}, 'Busqueda Alumno index', web);
			})
			.fail(function(){
				alert("Error en el Proceso");
			});
	});

initControls();

});

function initControls(){
window.location.hash="red";
window.location.hash="Red" //chrome
window.onhashchange=function(){window.location.hash="red";}
}